public class Application {
	
	public static void main(String[] args) {
		
		
		Capybara capy1 = new Capybara();
		
		capy1.diet = "pear";
		capy1.weight = 90;
		capy1.hairColor = "black";
		
		
		Capybara capy2 = new Capybara();
		
		capy2.diet = "apple";
		capy2.weight = 80;
		capy2.hairColor = "brown";
		
		Capybara[] herd = new Capybara[3];
		herd[0] = capy1;
		herd[1] = capy2;
		herd[2] = new Capybara();
		
		herd[2].diet = "apple";
		herd[2].weight = 85;
		herd[2].hairColor = "black";
		
		System.out.println("this is the diet of the first capybara: " + herd[0].diet);
		
		System.out.println("These are herd[2] features:");
		System.out.println("diet: " + herd[2].diet);
		System.out.println("weight: " + herd[2].weight);
		System.out.println("hair color: " + herd[2].hairColor);
		
		System.out.println("The diet of the first capybara is: " + capy1.diet);
		System.out.println("The weight of the first capybara is: " + capy1.weight);
		System.out.println("The hair color of the first capybara is: " + capy1.hairColor);
		
		System.out.println("The diet of the second capybara is: " + capy2.diet);
		System.out.println("The weight of the second capybara is: " + capy2.weight);
		System.out.println("The hair color of the second capybara is: " + capy2.hairColor);
		
		
		capy1.animalDiet(capy1.diet,capy1.hairColor);
		capy1.animalLifeSpan(capy1.weight,capy1.diet);
		capy2.animalDiet(capy2.diet,capy2.hairColor);
		capy2.animalLifeSpan(capy2.weight,capy2.diet);
		
		
	}
	
	
}
